# HelloFastAi

FastAI course demo project, inspired by www.fast.ai

## Usage

### Launch
```
gcloud compute ssh --zone=$ZONE jupyter@$INSTANCE_NAME -- -L 8080:localhost:8080
```
and then open `http://localhost:8080` in the browser.
